﻿#include <iostream>
#include <stdio.h>                 
#include <math.h>                

using namespace std;

double f(double x)
{
	return sin(pow(2, x) + 3);               //sin (2*x + 3) = 0, отрезок от 0 до 1
}
double findRoot(double a, double b, double e) // функция, которая ищет ноль на промежутке от a до b
{
	while (b - a > e) // пока не будет достигнута необходимая точность
	{
		if (f(a) * f((b + a) / 2) == 0)
			break;
		else if (f(a) * f((b + a) / 2) > 0)
			a = (b + a) / 2;
		else
			b = (b + a) / 2;
	}
	return (b + a) / 2;
}
int main() {
	double e = 1 * pow(10, -4); // e - это точность до 10^-4
	cout << findRoot(0, 1, e);
	return 0;
}